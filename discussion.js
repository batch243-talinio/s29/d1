// [Section] Comparison Queary Operator

	// $gt/$gte operator
		/*
			- allows us to find document that have field number value greater than or equal to specified value.

			syntax:
				db.collectionName.find({field: {$gt: value}});
				db.collectionName.find({field: {$gte: value}});
		*/

		// $gt
			db.users.find({age: {$gt: 50}}).pretty();
		// $gte
			db.users.find({age: {$gte: 21}}).pretty();


	// $lt/ $lte Operator
		/*
			- allows us ot find documents that have field number values less than or equal to a specified value.

			syntax:
				db.collectionName.find({field: {$lt: value}}).pretty();
				db.collectionName.find({field: {$lte: value}}).pretty();
		*/

		// $lt
			db.users.find({age: {$lt: 50}}).pretty();
		// $lte
			db.users.find({age: {$lte: 76}}).pretty();


	// $ne Operator
		/*
			- allows us to find documents that have field number values not equal to a specified value.

			syntax:
				db.collectionName.find({field: {$ne: value}}).pretty();
		*/

		// $ne
			db.users.find({age: {$ne: 68}}).pretty();


	// $in
		/*
			- allows us to find documents with specific values available inside.

			syntax:
				db.collectionName.find({field: {$ne: value}}).pretty();
		*/

		// $ne
			db.users.find({lastName: {$in: ["Doe", "Hawking"]}}).pretty();
			db.users.find({courses: {$in: ["CSS", "JavaScript"]}}).pretty();


// [Section] Logical Query Operators
	// $or operator
		/*
			- allows us to find documents that match a single criteria from multiple provided search criteria.

			syntax.
				db.collectionName.find({$or: [{fieldA: valueA}, {fieldB: ValueB}]});
		*/

		db.users.find({$or: [{firstName: "Neil"}, {age: {$gt: 30}}]}); // Neil armstrong and Stephen hawkings output.

	// $and Operator
		/*
			- allows us to find documents matching multiple provided criteria.

			syntax:
				db.collectionName.find({$and: [{fieldA: valueA}, {fieldB: ValueB}]});
		*/

		db.users.find({$and: [{firstName: "Neil"}, {age: {$gt: 30}}]}); // Neil armstrong output.


// [Section] Field Projection
	/*
		- retrieving documents are common that we do and by defualt mongoDB query return the whole document as a response
	*/

	// inclusion
		/*
			- it allows us to include/ add specific fields only when retrieving documents.

			syntax:
				db.users.find({criteriaA},{criteriaB});
		*/

		db.users.find({
			firstName: true, 
			lastName: 1, 	// true
			age: 1,			// true
		});

		db.users.find({$and: [{firstName: "Neil"}, {age: {$gt: 30}}]},
				{
					firstName: true, 
					lastName: 1, 	// true
					age: 1,			// true
				}
			);


	// Exclusion
		/*
			- allows us to exclude/ remove specific fields only when retrieving documents.
			- the value provided is 0.

			syntax:
				db.users.find({firstName: "Jane"}, 
					{
						contct: 0,
						department: 0,
					}
				);
		*/

		db.users.find({$and: [{firstName: "Neil"}, {age: {$gt: 25}}]},
			{
				age: false,
				courses: 0
			});

		// Returning Specific fields in EMbedded Documents

			db.users.find({firstName: "Jane"}, 
				{
					"contct.phone": 1,
					_id: 0 				// will not display ID.
					
				});
	//  !!	// make sure to stringigy it when accessing with .dot notation.

			db.users.find({firstName: "Jane"}, 
				{
					"contct.phone": 0, // false
					
				});


// [Section] Evaluation Queary Operators
	/*
		- it allows us to find documents that match a specific "STRING" pattern using regular expressions.

		syntax:
			db.users.find({field: {$regex: 'patternB', $options: $oprionValue}})
	*/

	// case sensitive queary
		db.users.find({firstName: {$regex: 'N'}});

	// case insensitive query - '$i' ignores casing
		db.users.find({firstName: {$regex: 'Ne', $options: '$i'}});


	// CRUD Operations
		db.users.updateOne({age: {$lte: 17}},
			{
				$set:{
					firstName: "Chris",
					lastName: "Mortel"
				}
			});

		db.users.deleteOne({$and: 
			[
				{firstName: "Chris"}, 
				{lastName: "Mortel"}
			]
		});